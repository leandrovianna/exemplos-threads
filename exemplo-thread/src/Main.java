
public class Main {

	public static void main(String[] args) {
		//Exemplo usando Thread
		//Contador extende Thread
		//Então ele herda o método run (onde fica a ação q sera feita)
		//e o start (que inicia a ação)
		//Contador então É UMA Thread, e então tem o método start para iniciar a tarefa
		
		Contador c1 = new Contador(1);
		c1.start();
		
		Contador c2 = new Contador(2);
		c2.start();
		
		Contador c3 = new Contador(3);
		c3.start();
		
		Contador c4 = new Contador(4);
		c4.start();

	}

}
