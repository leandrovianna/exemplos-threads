import javax.swing.JOptionPane;


public class MyDialog implements Runnable {

	private int id;
	
	public MyDialog(int id) {
		this.id = id;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Override
	public void run() {
		
		JOptionPane.showMessageDialog(null, 
				"Dialogo id: "+id, 
				"Dialogo "+id, 2);
		
	}
}
