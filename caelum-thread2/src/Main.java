
public class Main {

	public static void main(String[] args) {
		//Programa é uma classe que extende Runnable
		//e no método run imprime uma contagem de 1 a 50 acompanhado de seu id
		//Para identificar o programa nas diferentes Threads usamos esse id
		
		//criando a tarefa 1
		Programa p1 = new Programa();
		p1.setId(1);
		
		Thread t1 = new Thread(p1);
		t1.start();
		
		//criando a tarefa 2
		Programa p2 = new Programa();
		p2.setId(2);
		
		Thread t2 = new Thread(p2);
		t2.start();
		
		//criando a tarefa 3
		Programa p3 = new Programa();
		p3.setId(3);
		
		Thread t3 = new Thread(p3);
		t3.start();

	}

}
