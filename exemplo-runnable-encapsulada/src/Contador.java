
public class Contador implements Runnable {

	private int id;
	private Thread thread;

	public Contador(int id) {
		thread = new Thread(this);
		this.id = id;
	}
	
	@Override
	public void run() {
		for (int i = 1; i <= 100; i++)
			System.out.println("Contador nº:"+id+" Valor: "+i);
		
		System.out.println("=========================================");
	}
	
	public void iniciar() {
		thread.start();
	}
	
	public void parar() {
		thread.stop();
	}
}
