
public class Contador implements Runnable {

	private int id;

	public Contador(int id) {
		this.id = id;
	}
	
	@Override
	public void run() {
		for (int i = 1; i <= 100; i++)
			System.out.println("Contador nº:"+id+" Valor: "+i);
	}

	
	
}
