
public class Main {

	public static void main(String[] args) throws InterruptedException {
		//Exemplo usando Runnable
		//Contador implementa Runnable
		//Então ele implementa o método run (onde deve ficar a tarefa)
		//É necessário então criar um objeto Thread q recebe um
		//objeto com a interface Runnable (polimorfismo)
		//Thread recebe um objeto Runnable para garantir q a classe
		//tenha implementado o método run, q vai ser chamado pela Thread (contrato)
		
		//a tarefa é: imprimir de 1 a 100. Junto com valor, vem o id do contador
		//passado no construtor (para identificar os contadores e verificar a simultaniedade)
		
		Contador c1 = new Contador(1);
		Thread t1 = new Thread(c1);
		t1.start();
		
		Contador c2 = new Contador(2);
		Thread t2 = new Thread(c2);
		t2.start();
		
		Contador c3 = new Contador(3);
		Thread t3 = new Thread(c3);
		t3.start();
		
		Contador c4 = new Contador(4);
		Thread t4 = new Thread(c4);
		t4.start();

	}

}
